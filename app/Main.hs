module Main where

import Control.Contef ((:>))
import qualified Control.Contef.Except as Ex
import qualified Control.Contef.State as St
import qualified Control.Contef.Writer as Wr
import qualified Control.Contef.Reader as Re
import qualified Control.Contef.Nondet as No
import qualified Control.Contef.Lift as Lift
import qualified Control.Contef as Ef
import qualified MyTest

import qualified Control.Monad.IO.Class as MIO
import qualified Control.Monad.Reader as R
import qualified Control.Monad.State.Lazy as S
--import qualified Control.Monad.Writer.Lazy as W
import qualified Control.Monad.Except as E
import Data.Functor.Identity
import qualified TransTest
import qualified LiftTest

import qualified Control.Algebra as Fu
import qualified Control.Effect.Throw as FuEx
import qualified Control.Effect.Catch as Fu
--import qualified Control.Effect.State as FuSt
import qualified Control.Effect.Writer as Fu
import qualified Control.Effect.NonDet as Fu
--import qualified Control.Effect.Reader as FuRe
import qualified Control.Carrier.Throw.Either as FuExRun
import qualified Control.Carrier.Writer.Strict as Fu
import qualified Control.Carrier.State.Strict as FuStRun
import qualified Control.Carrier.NonDet.Church as Fu
import qualified Control.Carrier.Error.Either as Fu
--import qualified Control.Carrier.Reader as FuReRun
import qualified FusedTest

import qualified Polysemy as Po
import qualified Polysemy.State as PoSt
import qualified Polysemy.Error as PoEx
import qualified PolyTest

import qualified Control.Eff as ExtEf
import qualified Control.Eff.Exception as ExtEfEx
import qualified Control.Eff.State.Strict as ExtEfSt
--import qualified Control.Eff.Writer.Strict as ExtEfWr
--import qualified Control.Eff.Reader.Strict as ExtEfRe
import qualified ExtensibleTest

--import Data.Monoid (Sum (Sum))
--import Control.Monad (replicateM_)


testExcept :: Int -> Int -> Either String Int
testExcept x y =
  Ef.eval $
  Ex.run doRun
  where
    doRun :: forall ps. Ef.Efs '[Ex.Except String] ps => ps :> Int
    doRun = do
      x' <- g x
      y' <- g y
      pure (x' + y')

    g :: Ef.Has '[Ex.Except String] ss ps => Int -> ps :> Int
    g n =
      if n == 0
      then Ex.throw "zero"
      else pure n


partialStatePlus ::
  forall ss ps.
  Ef.Has '[St.State Int, Ex.Except String] ss ps =>
  Int -> Int -> ps :> Int
partialStatePlus m n = do
  check m
  St.put m
  check n
  m' <- St.get
  St.put (m' + n)
  r <- St.get
  pure r
  where
    check :: Int -> ps :> ()
    check x =
      if x == 0
      then Ex.throw "zero"
      else pure ()


partialStatePlusExSt :: Int -> Int -> Either String (Int, Int)
partialStatePlusExSt m n =
  Ef.eval $
  Ex.run $
  St.run (partialStatePlus m n) 0


partialStatePlusStEx :: Int -> Int -> (Int, Either String Int)
partialStatePlusStEx m n =
  Ef.eval $
  St.run (Ex.run (partialStatePlus m n)) 0


decr ::
  Ef.Has '[Ex.Except String, St.State Int] ss ps =>
  ps :> ()
decr = do
  x <- St.get @Int
  if x == 0
  then Ex.throw "zero"
  else St.put (x - 1)


tripleDecr ::
  Ef.Has '[St.State Int, Ex.Except String] ss ps =>
  ps :> String
tripleDecr =
  decr >> Ex.catch @String (decr >> decr >> pure "return") (\s -> pure ("catch: " ++ s))


tripleDecrStPa :: Int -> (Int, Either String String)
tripleDecrStPa i = Ef.eval $ St.run (Ex.run tripleDecr) i


tripleDecrPaSt :: Int -> Either String (Int, String)
tripleDecrPaSt i = Ef.eval $ Ex.run (St.run tripleDecr i)


localWriter ::
  Ef.Has '[Wr.Writer String, Re.Reader String] us ps => ps :> ()
localWriter = do
  Re.ask @String >>= Wr.tell @String
  Re.local @String (++" 1") (Re.ask @String >>= Wr.tell @String)
  Re.ask @String >>= Wr.tell @String
  Re.local @String (++" 2") $ do
    Re.ask @String >>= Wr.tell @String
    Re.local @String (++" 3") (Re.ask @String >>= Wr.tell @String)
    Re.ask @String >>= Wr.tell @String
  Re.ask @String >>= Wr.tell @String


localWriterWrRe :: String
localWriterWrRe =
  Ef.eval $ Wr.exec $ Re.run localWriter " 0"


localWriterReWr :: String
localWriterReWr =
  Ef.eval $ Re.run (Wr.exec localWriter) " 0"


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSols0 ::
  (Int, Int) -> Bool -> (Int, Int) ->
  Either String (Int, Int)
testAllSols0 (a, b) i (x, y) = do
  if (a * x + b * y == 0)
  then Right (x, y)
  else do
    if x + y > maxSum
    then Left "sum exceeded max"
    else
      if i
      then
        let r = testAllSols0 (a, b) (not i) (x + 1, y) in
        case r of
          Left e -> Left e
          Right (x', y') -> Right (x' + 1, y')
      else
        let r = testAllSols0 (a, b) (not i) (x, y + 1) in
        case r of
          Left e -> Left e
          Right (x', y') -> Right (x', y' + 1)


runTestAllSols0 :: (Int, Int) -> Either String (Int, Int)
runTestAllSols0 s = testAllSols0 (1, 1) False s


runTestAllSols :: (Int, Int) -> Either String (Int, Int)
runTestAllSols s =
  Ef.eval $
  Ex.run $
  St.eval (
    St.eval (
      MyTest.testAllSols (1, 1)
    ) s
  ) False


runTestAllSolsTrans :: (Int, Int) -> Either String (Int, Int)
runTestAllSolsTrans s =
  runIdentity $ 
  E.runExceptT $
  S.evalStateT (
    R.runReaderT (TransTest.testAllSolsTrans (1, 1)) s
  ) False


runTestAllSolsLift :: (Int, Int) -> Either String (Int, Int)
runTestAllSolsLift s =
  runIdentity $ 
  E.runExceptT $
  S.evalStateT (
    R.runReaderT (LiftTest.testAllSolsLift (1, 1)) s
  ) False


runTestAllSolsFused :: (Int, Int) -> Either String (Int, Int)
runTestAllSolsFused s =
  FuEx.run $
  FuExRun.runThrow $
  FuStRun.evalState False (
    FuStRun.evalState s (FusedTest.testAllSolsFused (1, 1))
  )


runTestAllSolsPoly :: (Int, Int) -> Either String (Int, Int)
runTestAllSolsPoly s =
  Po.run $
  PoEx.runError $
  PoSt.evalState False (
    PoSt.evalState s (PolyTest.testAllSolsPoly (1, 1))
  )


runTestAllSolsExtensible :: (Int, Int) -> Either String (Int, Int)
runTestAllSolsExtensible s =
  ExtEf.run $
  ExtEfEx.runError $
  ExtEfSt.evalState False $
  ExtEfSt.evalState s $
  ExtensibleTest.testAllSolsExtensible (1, 1)


testList1 :: [Int] -> [Int] -> [Int] -> (String, Either String [(Int, Int, Int)])
testList1 xs ys zs =
  Ef.eval $
  Wr.run $
  Ex.run @String $
  No.run $ do
    x <- No.choose xs
    Wr.tell (show x ++ " ")
    y <- Ex.catch @String (do
          y <- No.choose ys
          Wr.tell (show y ++ " ")
          pure y) (\_ -> pure 0)
    z <- No.choose zs
    Wr.tell (show z ++ " ")
    pure (x, y, z)


testList2 ::
  forall m.
  MIO.MonadIO m => [Int] -> [Int] -> [Int] ->
  m (Either String [(Int, Int, Int)])
testList2 xs ys zs =
  Lift.run $
  Ex.run @String $
  No.run $ do
    x <- No.choose xs
    Lift.liftIO @m $ putStr $ show x ++ " "
    y <- No.choose ys
    Lift.liftIO @m $ putStr $ show y ++ " "
    z <- No.choose zs
    Lift.liftIO @m $ putStr $ show z ++ " "
    pure (x, y, z)


testListPyth :: [Int] -> [Int] -> [Int] -> [(Int, Int, Int)]
testListPyth xs ys zs =
  Ef.eval $
  No.run $ do
    x <- No.choose xs
    y <- No.choose ys
    z <- No.choose zs
    if x*x + y*y == z*z then pure (x, y, z) else No.none


testList1Fused :: [Int] -> [Int] -> [Int] -> (String, Either String [(Int, Int, Int)])
testList1Fused xs ys zs =
  Fu.run $
  Fu.runWriter @String $
  Fu.runError @String $
  Fu.runNonDetA @[] $ do
    x <- Fu.oneOf xs
    Fu.tell (show x ++ " ")
    y <- Fu.catchError @String (do
          y <- Fu.oneOf ys
          Fu.tell (show y ++ " ")
          pure y) (\_ -> pure 0)
    z <- Fu.oneOf zs
    Fu.tell (show z ++ " ")
    pure (x, y, z)


runTestListPythFused :: [Int] -> [Int] -> [Int] -> [(Int, Int, Int)]
runTestListPythFused xs ys zs =
  Fu.run $
  Fu.runNonDetA @[] $ do
    x <- Fu.oneOf xs
    y <- Fu.oneOf ys
    z <- Fu.oneOf zs
    if x*x + y*y == z*z then pure (x, y, z) else Fu.empty

makeIntegerList :: Integer -> [Integer] -> [Integer]
makeIntegerList n acc =
  if n == 0 then acc else makeIntegerList (n - 1) (n : acc)


integerList :: [Integer]
integerList = makeIntegerList (10 * 10) []


timeTestChoice1 :: [(Integer, Integer, Integer)]
timeTestChoice1 =
  Ef.eval $
  St.eval (
    No.run $
    St.eval (
      No.choose integerList >>= \x ->
      No.choose integerList >>= \y ->
      No.choose integerList >>= \z ->
      St.get @Integer >>= \s ->
      St.put @[Int] (if s == 0 then [] else [1]) >>= \_ ->
      pure (x, y, x)
    ) (0 :: Integer)
  ) ([0] :: [Int])


timeTestChoice2 :: [(Integer, Integer, Integer)]
timeTestChoice2 =
  Ef.eval $
  St.eval (
    No.run $
    St.eval (
      No.choose integerList >>= \x ->
      No.choose integerList >>= \y ->
      No.choose integerList >>= \z ->
      St.get @Integer >>= \s ->
      St.put @[Int] (if s == 0 then [] else [1]) >>= \_ ->
      No.none
    ) (0 :: Integer)
  ) ([0] :: [Int])

main :: IO ()
main = do
{-
  print (testExcept 0 2)
  print (testExcept 2 0)
  print (testExcept 2 2)

  print (partialStatePlusExSt 0 2)
  print (partialStatePlusExSt 2 0)
  print (partialStatePlusExSt 2 2)

  print (partialStatePlusStEx 0 2)
  print (partialStatePlusStEx 2 0)
  print (partialStatePlusStEx 2 2)

  print (tripleDecrStPa 0)
  print (tripleDecrStPa 1)
  print (tripleDecrStPa 2)
  print (tripleDecrStPa 3)
  print (tripleDecrStPa 4)

  print (tripleDecrPaSt 0)
  print (tripleDecrPaSt 1)
  print (tripleDecrPaSt 2)
  print (tripleDecrPaSt 3)
  print (tripleDecrPaSt 4)

  putStrLn localWriterWrRe
  putStrLn localWriterReWr

-}
  let list = [1..100]

  --print (last $ fst (testList1 list list list))
  --print (length (runTestListPythFused list list list))
  --print (length (testListPyth list list list))
  print (length $ timeTestChoice1)
  print (length $ timeTestChoice2)

  --putStr "  "
  --x <- testList2 [1,2] [3,4] [5,6]
  --putStr "  "
  --print x

  --print (runTestAllSols0 (1, 1))
  --print (runTestAllSolsTrans (1, 1))
  --print (runTestAllSolsLift (1, 1))
  --print (runTestAllSols (1, 1))
  --print (runTestAllSolsFused (1, 1))
  --print (runTestAllSolsPoly (1, 1))
  --print (runTestAllSolsExtensible (1, 1))
