module SSS.Impl where

import Control.Monad (replicateM_)

import Control.Contef ((:>))
import qualified Control.Contef.State as St
import qualified Control.Contef.Writer as Wr
import qualified Control.Contef.Reader as Re
import qualified Control.Contef as Ef

import qualified Control.Eff as ExtEff
import qualified Control.Eff.State.Strict as ExtEff
import qualified Control.Eff.Writer.Strict as ExtEff
import qualified Control.Eff.Reader.Strict as ExtEff

import qualified Polysemy as Po
import qualified Polysemy.State as Po
import qualified Polysemy.Writer as Po
import qualified Polysemy.Reader as Po

import qualified Control.Effect.State as Fu
import qualified Control.Effect.Writer as Fu
import qualified Control.Effect.Reader as Fu

import qualified Control.Monad.Reader as MTL
import qualified Control.Monad.Writer.Lazy as MTL
import qualified Control.Monad.State.Lazy as MTL


iterations :: Int
iterations = foldl (*) (1 :: Int) (replicate 5 10)


contEf' ::
  forall ss ps.
  Ef.Has
  '[Re.Reader Int
  , St.State Int
  , Wr.Writer (MTL.Sum Int)] ss ps =>
  ps :> ()
contEf' = replicateM_ iterations $ do
  r <- Re.ask @Int
  Wr.tell (MTL.Sum r)
  s <- St.get @Int
  Wr.tell (MTL.Sum s)
  St.put (s + r)


newtype Contef =
  Contef
    ( forall ss ps.
      Ef.Has
      '[Re.Reader Int
      , St.State Int
      , Wr.Writer (MTL.Sum Int)] ss ps =>
      ps :> ()
    )


contEf :: Contef
contEf = Contef contEf'


extensibleEffects ::
  ( ExtEff.Member (ExtEff.Reader Int) r
  , ExtEff.Member (ExtEff.State Int) r
  , ExtEff.Member (ExtEff.Writer (MTL.Sum Int)) r) =>
  ExtEff.Eff r ()
extensibleEffects = replicateM_ iterations $ do
  r <- ExtEff.ask @Int
  ExtEff.tell (MTL.Sum r)
  s <- ExtEff.get @Int
  ExtEff.tell (MTL.Sum s)
  ExtEff.put (s + r)


polysemy ::
  (Po.Members
    '[Po.Reader Int
    , Po.State Int
    , Po.Writer (MTL.Sum Int)] r) =>
  Po.Sem r ()
polysemy = replicateM_ iterations $ do
  r <- Po.ask @Int
  Po.tell (MTL.Sum r)
  s <- Po.get @Int
  Po.tell (MTL.Sum s)
  Po.put (s + r)


fusedEffects' ::
  ( Fu.Has (Fu.Reader Int) sig m
  , Fu.Has (Fu.State Int) sig m
  , Fu.Has (Fu.Writer (MTL.Sum Int)) sig m) =>
  m ()
fusedEffects' = replicateM_ iterations $ do
  r <- Fu.ask @Int
  Fu.tell (MTL.Sum r)
  s <- Fu.get @Int
  Fu.tell (MTL.Sum s)
  Fu.put (s + r)


newtype FusedEffects =
  FusedEffects
    ( forall sig m.
      ( Fu.Has (Fu.Reader Int) sig m
      , Fu.Has (Fu.State Int) sig m
      , Fu.Has (Fu.Writer (MTL.Sum Int)) sig m) =>
      m ()
    )


fusedEffects :: FusedEffects
fusedEffects = FusedEffects fusedEffects'


mtl ::
  (MTL.MonadReader Int m,
    MTL.MonadState Int m,
    MTL.MonadWriter (MTL.Sum Int) m) =>
  m ()
mtl = replicateM_ iterations $ do
  r <- MTL.ask
  MTL.tell (MTL.Sum r)
  s <- MTL.get
  MTL.tell (MTL.Sum s)
  MTL.put (s + r)
