module SSS.Run
  ( runContEf
  , contEf
  , runExtensibleEffects
  , extensibleEffects
  , runPolysemy
  , polysemy
  , runFusedEffects
  , fusedEffects
  , runMtl
  , mtl
  ) where

import SSS.Impl

import qualified Control.Contef.State as St
import qualified Control.Contef.Writer as Wr
import qualified Control.Contef.Reader as Re
import qualified Control.Contef.RWS as RWS
import qualified Control.Contef as Ef

import qualified Control.Eff as ExtEff
import qualified Control.Eff.State.Strict as ExtEff
import qualified Control.Eff.Writer.Strict as ExtEff
import qualified Control.Eff.Reader.Strict as ExtEff

import qualified Polysemy as Po
import qualified Polysemy.State as Po
import qualified Polysemy.Writer as Po
import qualified Polysemy.Reader as Po

import qualified Control.Carrier.State.Strict as Fu
import qualified Control.Carrier.Writer.Strict as Fu
import qualified Control.Carrier.Reader as Fu

import qualified Control.Monad.Reader as MTL
import qualified Control.Monad.Writer.Lazy as MTL
import qualified Control.Monad.State.Lazy as MTL


runContEf :: Contef -> (Int, MTL.Sum Int)
runContEf (Contef e) =
  Ef.eval $
  Re.run (
    St.run (
      Wr.exec e
    ) (0 :: Int)
  ) (1 :: Int)

{-
runContEf :: Contef -> (MTL.Sum Int, Int, ())
runContEf (Contef e) =
  Ef.eval $ RWS.run e (1 :: Int) (0 :: Int)
-}


runExtensibleEffects ::
  (ExtEff.Eff
    '[ExtEff.Writer (MTL.Sum Int)
    , ExtEff.State Int
    , ExtEff.Reader Int] ()) ->
  (MTL.Sum Int, Int)
runExtensibleEffects e =
  ExtEff.run $
  ExtEff.runReader (1 :: Int) $
  ExtEff.runState (0 :: Int) $
  ExtEff.execWriter mappend mempty $
  e


runPolysemy ::
  (Po.Sem
    '[Po.Writer (MTL.Sum Int)
    , Po.State Int
    , Po.Reader Int] ()) ->
  (Int, MTL.Sum Int)
runPolysemy e =
  Po.run $
  Po.runReader (1 :: Int) $
  Po.runState (0 :: Int) $
  fmap fst $ Po.runWriter e


runFusedEffects :: FusedEffects -> (Int, MTL.Sum Int)
runFusedEffects (FusedEffects e) =
  Fu.run $
  Fu.runReader (1 :: Int) $
  Fu.runState (0 :: Int) $
  fmap fst $
  Fu.runWriter @(MTL.Sum Int) e


runMtl ::
  MTL.WriterT (MTL.Sum Int) (MTL.StateT Int (MTL.Reader Int)) () ->
  (MTL.Sum Int, Int)
runMtl =
  flip MTL.runReader 1
  . flip MTL.runStateT 0
  . MTL.execWriterT
