module Main(main) where

import Criterion.Main

import qualified SSS.Run as SSS


main :: IO ()
main = defaultMain
  [ bgroup "SSS"
    [ bench "polysemy" $ nf SSS.runPolysemy SSS.polysemy
    , bench "extensible effects" $ nf SSS.runExtensibleEffects SSS.extensibleEffects
    , bench "MTL" $ nf SSS.runMtl SSS.mtl
    , bench "cont-ef" $ nf SSS.runContEf SSS.contEf
    , bench "fused effects" $ nf SSS.runFusedEffects SSS.fusedEffects
    ]
  ]
