module LiftTest (testAllSolsLift) where

import qualified Control.Monad.Reader as R
import qualified Control.Monad.State.Lazy as S
import qualified Control.Monad.Except as E
import Control.Monad.Trans (lift)
import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSolsLift :: Monad m =>
  (Int, Int) ->
  R.ReaderT (Int, Int) (S.StateT Bool (E.ExceptT String m)) (Int, Int)
testAllSolsLift (x, y) = do
  (a, b) <- R.ask
  if (a * x + b * y == 0)
  then pure (x, y)
  else do
    when (x + y > maxSum) (lift . lift $ E.throwError "sum exceeded max")
    i <- lift S.get
    lift (S.put (not i))
    if i
    then testAllSolsLift (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSolsLift (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
