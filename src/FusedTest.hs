module FusedTest (testAllSolsFused) where

import qualified Control.Effect.Throw as FuEx
import qualified Control.Effect.State as FuSt
import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSolsFused ::
    -- Global state constant (A, B)
  (FuEx.Has (FuSt.State (Int, Int)) sig m,
    FuEx.Has (FuSt.State Bool) sig m,
    FuEx.Has (FuEx.Throw String) sig m) =>
  -- Value to start search from.
  (Int, Int) ->
  m (Int, Int) -- (x, y) such that Ax + By = 0
testAllSolsFused (x, y) = do
  (a, b) <- FuSt.get @(Int, Int)
  if a * x + b * y == 0
  then pure (x, y)
  else do
    when (x + y > maxSum) (FuEx.throwError "sum exceeded max")
    i <- FuSt.get @Bool
    FuSt.put (not i)
    if i
    then testAllSolsFused (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSolsFused (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
