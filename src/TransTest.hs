module TransTest (testAllSolsTrans) where

import qualified Control.Monad.Reader as R
import qualified Control.Monad.State.Lazy as S
import qualified Control.Monad.Except as E
import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSolsTrans ::
  (R.MonadReader (Int, Int) m,
    S.MonadState Bool m,
    E.MonadError String m) =>
  (Int, Int) ->
  m (Int, Int)
testAllSolsTrans (x, y) = do
  (a, b) <- R.ask
  if (a * x + b * y == 0)
  then pure (x, y)
  else do
    when (x + y > maxSum) (E.throwError "sum exceeded max")
    i <- S.get
    S.put (not i)
    if i
    then testAllSolsTrans (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSolsTrans (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
