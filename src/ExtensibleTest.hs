module ExtensibleTest (testAllSolsExtensible) where

import qualified Control.Eff as ExtEf
import qualified Control.Eff.Exception as ExtEfEx
import qualified Control.Eff.State.Strict as ExtEfSt
import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSolsExtensible ::
  (ExtEf.Member (ExtEfSt.State (Int, Int)) r,
    ExtEf.Member (ExtEfSt.State Bool) r,
    ExtEf.Member (ExtEfEx.Exc String) r) =>
  (Int, Int) ->
  ExtEf.Eff r (Int, Int)
testAllSolsExtensible (x, y) = do
  (a, b) <- ExtEfSt.get @(Int, Int)
  if (a * x + b * y == 0)
  then pure (x, y)
  else do
    when (x + y > maxSum) (ExtEfEx.throwError "sum exceeded max")
    i <- ExtEfSt.get @Bool
    ExtEfSt.put (not i)
    if i
    then testAllSolsExtensible (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSolsExtensible (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
