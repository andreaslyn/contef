module PolyTest (testAllSolsPoly) where


import qualified Polysemy as Po
import qualified Polysemy.State as PoSt
import qualified Polysemy.Error as PoEx
import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSolsPoly ::
  Po.Members '[PoSt.State (Int, Int), PoSt.State Bool, PoEx.Error String] r =>
  (Int, Int) ->
  Po.Sem r (Int, Int)
testAllSolsPoly (x, y) = do
  (a, b) <- PoSt.get @(Int, Int)
  if a * x + b * y == 0
  then pure (x, y)
  else do
    when (x + y > maxSum) (PoEx.throw "sum exceeded max")
    i <- PoSt.get @Bool
    PoSt.put (not i)
    if i
    then testAllSolsPoly (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSolsPoly (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
