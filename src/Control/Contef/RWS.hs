module Control.Contef.RWS where
import Control.Contef (Ef, Efs, (:>), (:+:))
import qualified Control.Contef as Ef

import Control.Contef.Reader (Reader)
import qualified Control.Contef.Reader as Re

import Control.Contef.Writer (Writer)
import qualified Control.Contef.Writer as Wr

import Control.Contef.State (State)
import qualified Control.Contef.State as St


type RWS (r :: *) (w :: *) (s :: *) = (Reader r :+: Writer w) :+: State s


newtype Env (w :: *) (s :: *) (a :: *) = Env (w, s, a)


prEnv :: Env w s a -> a
prEnv (Env (_, _, a)) = a


instance Functor (Env w s) where
  fmap f (Env (w, s, a)) = Env (w, s, f a)


newtype RWSC (r :: *) (w :: *) (s :: *) (ps :: [*]) (a :: *) =
  RWSC (r -> s -> ps :> Env w s a)


runRWSC :: r -> s -> RWSC r w s ps a -> ps :> Env w s a
runRWSC r s (RWSC f) = f r s


instance Monoid w => Ef.Carrier us (RWSC r w s) where
  type Env us (RWSC r w s) = Env w s
  
  modifyEnv scope k =
    pure $ RWSC $ \r s -> do
      Env (w, s', x) <- scope (Env (mempty, s, ())) (>>= runRWSC r s)
      k' <- k x
      Env (w', s'', x') <- runRWSC r s' k'
      let !w'' = mappend w w'
      pure (Env (w'', s'', x'))

  unitCarrier a = pure (RWSC $ \_ s -> pure (Env (mempty, s, a)))
  {-# INLINE unitCarrier #-}


instance Monoid w => Ef.Handler (RWS r w s) us (RWSC r w s) where
  shift (Ef.Sigl (Ef.Sigl Re.Ask)) k =
    pure $ RWSC $ \r s -> k r >>= runRWSC r s
  shift (Ef.Sigl (Ef.Sigr (Wr.Tell w))) k =
    pure $ RWSC $ \r s -> do
      Env (w', s', x) <- k () >>= runRWSC r s
      let !w'' = mappend w w'
      pure (Env (w'', s', x))
  shift (Ef.Sigr St.Get) k =
    pure $ RWSC $ \r s -> k s >>= runRWSC r s
  shift (Ef.Sigr (St.Put s)) k =
    pure $ RWSC $ \r _ -> k () >>= runRWSC r s
  {-# INLINE shift #-}

  reset (Ef.Sigl (Ef.Sigl (Re.Local m))) (f Ef.:& Ef.Nildom) _ k =
    pure $ RWSC $ \r s ->
      Ef.getNest f >>= runRWSC (m r) s >>= k . prEnv >>= runRWSC r s
  reset (Ef.Sigl (Ef.Sigr Wr.Intercept)) (scope Ef.:& Ef.Nildom) _ k =
    pure $ RWSC $ \r s -> do
      Env (w, s', x) <- Ef.getNest scope >>= runRWSC r s
      k' <- k (fmap ((,) w) x)
      runRWSC r s' k'
  reset (Ef.Sigr x) _ _ _ = case x of {}
  {-# INLINE reset #-}


run ::
  forall r w s us ps a. (Monoid w, Efs us ps) =>
  (forall rws. Ef (RWS r w s) us rws => rws : ps :> a) -> r -> s -> ps :> (w, s, a)
run c !r !s = Ef.coerce (Ef.handle @_ @_ @(RWSC r w s) c >>= runRWSC r s)
{-# INLINE run #-}
