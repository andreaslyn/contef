module Control.Contef.State
  ( State (..)
  , StateC (..)
  , get
  , put
  , modify
  , gets
  , run
  , eval
  , exec
  ) where

import Control.Contef (Ef, Efs, Has, (:>))
import qualified Control.Contef as Ef


data State :: * -> Ef.Signa where
  Get :: forall s. State s Ef.Shift s
  Put :: forall s. s -> State s Ef.Shift ()


get :: forall s us ps. Has '[State s] us ps => ps :> s
get = Ef.performShift Get
{-# INLINE get #-}


put :: forall s us ps. Has '[State s] us ps => s -> ps :> ()
put = Ef.performShift . Put
{-# INLINE put #-}


modify :: forall s us ps. Has '[State s] us ps => (s -> s) -> ps :> ()
modify f = get >>= put . f
{-# INLINE modify #-}


gets :: forall s us ps a. Has '[State s] us ps => (s -> a) -> ps :> a
gets f = fmap f get
{-# INLINE gets #-}


newtype StateC (s :: *) (ps :: [*]) (a :: *) = StateC (s -> ps :> (s, a))


runStateC :: forall s ps a. s -> StateC s ps a -> ps :> (s, a)
runStateC s = \(StateC f) -> f s
{-# INLINE runStateC #-}


instance Ef.Carrier us (StateC s) where
  type Env us (StateC s) = (,) s
  
  modifyEnv scope k =
    pure $ StateC $ \s -> do
      (s', x) <- scope (s, ()) (>>= runStateC s)
      k' <- k x
      runStateC s' k'

  unitCarrier a = pure (StateC $ \s -> pure (s, a))
  {-# INLINE unitCarrier #-}


instance Ef.Handler (State s) us (StateC s) where
  shift Get k = pure $ StateC $ \s -> k s >>= runStateC s
  shift (Put !s) k = pure $ StateC $ \_ -> k () >>= runStateC s
  {-# INLINE shift #-}

  reset x _ _ _ = case x of {}


run ::
  forall s us ps a. Efs us ps =>
  (forall st. Ef (State s) us st => st : ps :> a) -> s -> ps :> (s, a)
run c !s = Ef.handle c >>= runStateC s
{-# INLINE run #-}


eval ::
  forall s us ps a. Efs us ps =>
  (forall st. Ef (State s) us st => st : ps :> a) -> s -> ps :> a
eval c s = fmap snd (run c s)
{-# INLINE eval #-}


exec ::
  forall s us ps a. Efs us ps =>
  (forall st. Ef (State s) us st => st : ps :> a) -> s -> ps :> s
exec c s = fmap fst (run c s)
{-# INLINE exec #-}
