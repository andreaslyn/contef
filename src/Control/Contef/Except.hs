module Control.Contef.Except
  ( Except (..)
  , ExceptC (..)
  , throw
  , run
  , catch
  , liftEither
  ) where

import Control.Contef (Ef, Efs, Has, (:>))
import qualified Control.Contef as Ef

import Data.Functor (($>))


data Except :: * -> Ef.Signa where
  Throw :: forall e a. e -> Except e Ef.Shift a
  Catch :: forall e a. Except e (Ef.Reset1 a) (Either e a)


newtype ExceptC e (ps :: [*]) a = ExceptC { runExceptC :: Either e a }


instance Ef.Carrier us (ExceptC e) where
  type Env us (ExceptC e) = Either e

  modifyEnv scope k = do
    x <- scope (Right ()) Ef.coerce
    case x of
      Left e -> pure (ExceptC (Left e))
      Right x' -> k x'

  unitCarrier = pure . ExceptC . Right
  {-# INLINE unitCarrier #-}


throw :: forall e us ps a. Has '[Except e] us ps => e -> ps :> a
throw = Ef.performShift . Throw
{-# INLINE throw #-}


catchEither ::
  forall e us ps a. Has '[Except e] us ps =>
  (forall qs. Efs us qs => qs :> a) -> ps :> Either e a
catchEither s =
  Ef.performReset Catch $ Ef.Scope s Ef.:& Ef.Nildom
{-# INLINE catchEither #-}

-- throw e >>= k  =  throw e
-- catchEither (throw e) >>= k = k (Left e)
-- catchEither (pure x) >>= k = k (Right x)


catch ::
  forall e us ps a.
  Has '[Except e] us ps =>
  (forall qs. Efs us qs => qs :> a) ->
  (e -> ps :> a) ->
  ps :> a
catch a h = catchEither a >>= either h pure
{-# INLINE catch #-}


instance Ef.Handler (Except e) us (ExceptC e) where
  shift (Throw e) _ = pure (ExceptC (Left e))
  {-# INLINE shift #-}

  reset Catch (ex Ef.:& Ef.Nildom) rb k =
    Ef.getNest ex >>= \case
      ExceptC (Left e) -> k $ rb $> Left e
      ExceptC (Right x) -> k $ fmap Right x
  {-# INLINE reset #-}


run ::
  forall e us ps a. Efs us ps =>
  (forall ex. Ef (Except e) us ex => ex : ps :> a) ->
  ps :> Either e a
run c = Ef.coerce (Ef.handle @_ @_ @(ExceptC e) c)
{-# INLINE run #-}


liftEither ::
  forall e us ps a.
  Has '[Except e] us ps => Either e a -> ps :> a
liftEither (Left e) = throw e
liftEither (Right a) = pure a
{-# INLINE liftEither #-}
