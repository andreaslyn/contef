{-# LANGUAGE UndecidableInstances #-} -- for Efs instance constraint

module Control.Contef.Internal.Contef
  ( Ef (..)
  , IsEf (..)
  , MkEf
  , AreEfs (..)
  , Efs (..)
  , PreComp
  , (:>) (..)
  , Carrier (..)
  , RunCarrier
  , Scope (..)
  , Nest (..)
  , coerceDomToIdentity
  , Handler (..)
  ) where

import Control.Contef.Internal.Signa (Signa, Arity, Operation (..), Dom)

import Control.Monad.Trans.Cont (ContT)
import Data.Functor.Identity (Identity)

import Data.Functor.Compose (Compose)
import Unsafe.Coerce (unsafeCoerce)


data MkEf (u :: Signa) (t :: [*] -> * -> *) (r :: *)


data IsEf :: Signa -> [Signa] -> * -> * where
  IsEf :: forall u us t r. Handler u us t => IsEf u us (MkEf u t r)


class Ef (u :: Signa) (us :: [Signa]) (p :: *) | p -> u where
  isEf :: IsEf u us p


instance Handler u us t => Ef (u :: Signa) (us :: [Signa]) (MkEf u t r) where
  isEf = IsEf
  {-# INLINE isEf #-}


data AreEfs :: [Signa] -> [*] -> * where
  ConsAreEfs ::
    forall u us t r ps. (Efs us ps, Handler u us t) =>
    AreEfs (u : us) (MkEf u t r : ps)
  NilAreEfs :: AreEfs '[] '[]


class Efs (us :: [Signa]) (ps :: [*]) | ps -> us where
  areEfs :: AreEfs us ps


instance Efs '[] '[] where
  areEfs = NilAreEfs
  {-# INLINE areEfs #-}


instance (Ef u us p, Efs us ps) => Efs (u : us) (p : ps) where
  areEfs = aux isEf where
    aux :: IsEf u us p -> AreEfs (u : us) (p : ps)
    aux IsEf = ConsAreEfs
  {-# INLINE areEfs #-}


type family PreComp (ps :: [*]) :: * -> * where
  PreComp '[] = Identity
  PreComp (MkEf u t r : ps) = ContT (t ps r) ((:>) ps)


infixr 0 :>

newtype (ps :: [*]) :> (a :: *) = Comp { run :: PreComp ps a }


type RunCarrier (us :: [Signa]) (t :: [*] -> * -> *) =
  forall x qs. Efs us qs => (qs :> t qs x) -> qs :> Env us t x


class Carrier (us :: [Signa]) (t :: [*] -> * -> *) where
  type Env us t :: * -> *

  unitCarrier :: forall (a :: *) (ps :: [*]). Efs us ps => a -> ps :> t ps a

  modifyEnv ::
    forall (a :: *) (b :: *) (ps :: [*]). Efs us ps =>
    (Env us t () -> RunCarrier us t -> ps :> Env us t a) ->
    (a -> ps :> t ps b) ->
    ps :> t ps b

{- Laws:
modifyEnv (\_ r -> r (unitCarrier x)) h = h x
modifyEnv (\_ r -> r s) unitCarrier = s
modifyEnv (\e _ -> pure (e $> x)) h = h x
modifyEnv (\e r -> fmap (fmap g) (f e r)) h = modifyEnv f (h . g)
-}


-- Try to get rid of Scope and Nest. Maybe by having two
-- different dom types, one for each.
newtype Scope (us :: [Signa]) (a :: *) =
  Scope { getScope :: forall (ps :: [*]). Efs us ps => ps :> a }

-- TODO: Get rid of this:
coerceDomToIdentity :: Dom f n -> Dom (f `Compose` Identity) n
coerceDomToIdentity = unsafeCoerce


newtype Nest (t :: [*] -> * -> *) (f :: * -> *) (ps :: [*]) (a :: *) =
  Nest { getNest :: ps :> t ps (f a) }


class (Carrier us t, Functor (Env us t)) =>
      Handler (u :: Signa) (us :: [Signa]) (t :: [*] -> * -> *) where
  shift ::
    forall (a :: *) (r :: *) (ps :: [*]). Efs us ps =>
    u 'Shift a -> (a -> ps :> t ps r) -> ps :> t ps r

  reset ::
    forall (f :: * -> *) (b :: *) (n :: Arity) (r :: *) (ps :: [*]).
    (Efs us ps, Functor f) =>
    u ('Reset n) b ->
    Dom (Nest t f ps) n ->
    f () ->
    (f b -> ps :> t ps r) ->
    ps :> t ps r

{- The laws for algebras are given by the signature u -}
