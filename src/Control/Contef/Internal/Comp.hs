{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE UndecidableInstances #-} -- for Efs instance requirement

module Control.Contef.Internal.Comp
  ( (:>) (..)
  , eval
  , evalEfs
  , getIdentity
  , getCont
  , coerce
  ) where

import Control.Contef.Internal.Signa (Signa)
import Control.Contef.Internal.Ef (Efs, MkEf, AreEfs (..), Efs (..))
import Control.Contef.Internal.Contef ((:>) (..))

import Control.Monad.Trans.Cont (ContT)

import Data.Functor.Identity (Identity, runIdentity)

import Data.Coerce (Coercible)
import Unsafe.Coerce (unsafeCoerce)


eval :: forall (r :: *). '[] :> r -> r
eval (Comp a) = runIdentity a
{-# INLINE eval #-}


evalEfs :: forall (r :: *) (ps :: [*]). Efs '[] ps => (ps :> r) -> r
evalEfs = evalAreEfs areEfs
{-# INLINE evalEfs #-}


evalAreEfs :: forall (r :: *) (ps :: [*]). AreEfs '[] ps -> (ps :> r) -> r
evalAreEfs NilAreEfs = eval
{-# INLINE evalAreEfs #-}


getIdentity :: forall (a :: *). '[] :> a -> Identity a
getIdentity (Comp a) = a
{-# INLINE getIdentity #-}


getCont ::
  forall
    (u :: Signa) (t :: [*] -> * -> *) (r :: *) (ps :: [*]) (a :: *).
  MkEf u t r : ps :> a -> ContT (t ps r) ((:>) ps) a
getCont (Comp a) = a
{-# INLINE getCont #-}


fmapComp ::
  forall (us :: [Signa]) (ps :: [*]) (a :: *) (b :: *).
  AreEfs us ps -> (a -> b) -> ps :> a -> ps :> b
fmapComp ConsAreEfs = \f (Comp a) -> Comp (fmap f a)
fmapComp NilAreEfs = \f (Comp a) -> Comp (fmap f a)
{-# INLINE fmapComp #-}


instance Efs us ps => Functor ((:>) ps) where
  fmap = fmapComp areEfs
  {-# INLINE fmap #-}


pureComp ::
  forall (us :: [Signa]) (ps :: [*]) (a :: *).
  AreEfs us ps -> a -> ps :> a
pureComp ConsAreEfs = Comp . pure
pureComp NilAreEfs = Comp . pure
{-# INLINE pureComp #-}


apComp ::
  forall (us :: [Signa]) (ps :: [*]) (a :: *) (b :: *).
  AreEfs us ps -> ps :> (a -> b) -> ps :> a -> ps :> b
apComp ConsAreEfs = \(Comp f) (Comp a) -> Comp (f <*> a)
apComp NilAreEfs = \(Comp f) (Comp a) -> Comp (f <*> a)
{-# INLINE apComp #-}


instance Efs us ps => Applicative ((:>) ps) where
  pure = pureComp areEfs
  {-# INLINE pure #-}
  (<*>) = apComp areEfs
  {-# INLINE (<*>) #-}


bindComp ::
  forall (us :: [Signa]) (ps :: [*]) (a :: *) (b :: *).
  AreEfs us ps -> ps :> a -> (a -> ps :> b) -> ps :> b
bindComp ConsAreEfs = \(Comp a) f -> Comp (a >>= getCont . f)
bindComp NilAreEfs = \(Comp a) f -> Comp (a >>= getIdentity . f)
{-# INLINE bindComp #-}


instance Efs us ps => Monad ((:>) ps) where
  (>>=) = bindComp areEfs
  {-# INLINE (>>=) #-}


coerce ::
  forall (a :: *) (b :: *) (ps :: [*]). Coercible a b =>
  ps :> a -> ps :> b
coerce = unsafeCoerce
{-# INLINE coerce #-}
