module Control.Contef.Internal.Ef
  ( Ef (..)
  , IsEf (..)
  , MkEf
  , AreEfs (..)
  , Efs (..)
  , Has
  ) where

import Control.Contef.Internal.Signa (Signa, Sub)

import Control.Contef.Internal.Contef
  ( Ef (..)
  , IsEf (..)
  , MkEf
  , AreEfs (..)
  , Efs (..)
  )


type family Has (xs :: [Signa]) (us :: [Signa]) (ps :: [*]) where
  Has xs us ps = (Efs us ps, Sub xs us)
