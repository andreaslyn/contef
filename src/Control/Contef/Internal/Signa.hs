{-# LANGUAGE UndecidableInstances #-}

module Control.Contef.Internal.Signa
  ( Arity (..), Arity1, Arity2, Arity3
  , Operation (..), Shift, Reset1, Reset2, Reset3
  , Dom (..)
  , mapDom
  , Signa
  , (:+:) (..)
  , Inject (..)
  , IsIn (..)
  , In (..)
  , Sub
  ) where

import Data.Kind (Constraint)


data Arity :: * where
  Nullary :: Arity
  Succary :: * -> Arity -> Arity


type Arity1 a1 = 'Succary a1 'Nullary

type Arity2 a1 a2 = 'Succary a1 (Arity1 a2)

type Arity3 a1 a2 a3 = 'Succary a1 (Arity2 a2 a3)


data Operation = Shift | Reset Arity


type Shift = 'Shift

type Reset1 a1 = 'Reset (Arity1 a1)

type Reset2 a1 a2 = 'Reset (Arity2 a2 a2)

type Reset3 a1 a2 a3 = 'Reset (Arity3 a1 a2 a3)


infixr 5 :&

data Dom :: (* -> *) -> Arity -> * where
  Nildom :: forall (f :: * -> *). Dom f 'Nullary
  (:&) ::
    forall (f :: * -> *) (n :: Arity) (a :: *).
    f a -> Dom f n -> Dom f ('Succary a n)


mapDom ::
  forall (f :: * -> *) (g :: * -> *) (n :: Arity).
  (forall a. f a -> g a) -> Dom f n -> Dom g n
mapDom _ Nildom = Nildom
mapDom m (x :& xs) = (m x) :& mapDom m xs


type Signa = Operation -> * -> *


infixr 2 :+:

data ((u :: Signa) :+: (v :: Signa)) (n :: Operation) (a :: *) =
  Sigl (u n a) | Sigr (v n a)


class Inject (u :: Signa) (v :: Signa) where
  inject :: forall (n :: Operation) (a :: *). u n a -> v n a


instance Inject u u where
  inject = id
  {-# INLINE inject #-}


instance {-# OVERLAPPABLE #-} Inject u (u :+: v) where
  inject = Sigl
  {-# INLINE inject #-}


instance {-# OVERLAPPABLE #-} Inject u w => Inject u (v :+: w) where
  inject = Sigr . inject
  {-# INLINE inject #-}


reassocInject ::
  forall u n v1 v2 w a. Inject u (v1 :+: (v2 :+: w)) =>
  u n a -> ((v1 :+: v2) :+: w) n a
reassocInject a =
  case inject a of
    Sigl v1 -> Sigl (Sigl v1)
    Sigr (Sigl v2) -> Sigl (Sigr v2)
    Sigr (Sigr w) -> Sigr w
{-# INLINE reassocInject #-}


instance {-# OVERLAPPABLE #-} 
         Inject u (v1 :+: (v2 :+: w)) =>
         Inject u ((v1 :+: v2) :+: w) where
  inject = reassocInject
  {-# INLINE inject #-}


data IsIn :: Signa -> [Signa] -> * where
  There ::
    forall (u :: Signa) (us :: [Signa]) (v :: Signa).
    !(IsIn u us) -> IsIn u (v : us)
  Here ::
    forall (u :: Signa) (us :: [Signa]).
    IsIn u (u : us)
  Inject ::
    forall (u :: Signa) (v :: Signa) (us :: [Signa]).
    !(forall n a. u n a -> v n a) ->
    IsIn u (v : us)


class In (u :: Signa) (us :: [Signa]) where
  isIn :: IsIn u us


instance In u (u : us) where
  isIn = Here
  {-# INLINE isIn #-}


instance {-# OVERLAPPABLE #-} In u us => In u (v : us) where
  isIn = There isIn
  {-# INLINE isIn #-}


instance {-# OVERLAPPABLE #-} In u ((u :+: v) : us) where
  isIn = Inject Sigl
  {-# INLINE isIn #-}


instance {-# OVERLAPPABLE #-} Inject u w => In u ((v :+: w) : us) where
  isIn = Inject (Sigr . inject)
  {-# INLINE isIn #-}


instance {-# OVERLAPPABLE #-}
         Inject u (v1 :+: (v2 :+: w)) =>
         In u (((v1 :+: v2) :+: w) : us) where
  isIn = Inject reassocInject
  {-# INLINE isIn #-}


type family Sub (xs :: [Signa]) (us :: [Signa]) :: Constraint where
  Sub '[] us = ()
  Sub (x : xs) us = (In x us, Sub xs us)
