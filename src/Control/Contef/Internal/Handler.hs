module Control.Contef.Internal.Handler
  ( Carrier (..)
  , RunCarrier
  , Scope (..)
  , Nest (..)
  , Handler (..)
  , handle
  , performShift
  , performReset
  ) where

import Control.Contef.Internal.Signa (Signa)
import qualified Control.Contef.Internal.Signa as Sig

import Control.Contef.Internal.Ef (Ef, Efs, Has)
import qualified Control.Contef.Internal.Ef as Ef

import Control.Contef.Internal.Comp ((:>) (Comp))
import qualified Control.Contef.Internal.Comp as Com

import qualified Control.Contef.Internal.Contef as Contef
import Control.Contef.Internal.Contef
  ( Carrier (..)
  , RunCarrier
  , Scope (..)
  , Nest (..)
  , Handler (..)
  )

import Control.Monad.Trans.Cont (ContT (..))

import Data.Functor (($>))
import Data.Functor.Identity (Identity (Identity))
import Data.Functor.Compose (Compose (Compose))


handle ::
  forall (u :: Signa) (us :: [Signa]) (t :: [*] -> * -> *) (r :: *) (ps :: [*]).
  (Efs us ps, Handler u us t) =>
  (forall p. Ef u us p => p : ps :> r) ->
  ps :> t ps r
handle c = runContT (Com.getCont c) unitCarrier
{-# INLINE handle #-}


performShift ::
  forall (u :: Signa) (us :: [Signa]) (ps :: [*]) (a :: *).
  Has '[u] us ps => u 'Sig.Shift a -> ps :> a
performShift op = liftShift op Sig.isIn Ef.areEfs
{-# INLINE performShift #-}


liftShift ::
  forall (u :: Signa) (us :: [Signa]) (ps :: [*]) (a :: *).
  u 'Sig.Shift a -> Sig.IsIn u us -> Ef.AreEfs us ps -> ps :> a
liftShift op (Sig.There h) Ef.ConsAreEfs =
  Comp $ ContT $ \f -> liftShift op h Ef.areEfs >>= f
liftShift op Sig.Here Ef.ConsAreEfs =
  Comp $ ContT $ \f -> shift op f
liftShift op (Sig.Inject inj) Ef.ConsAreEfs =
  Comp $ ContT $ \f -> shift (inj op) f
{-# INLINABLE liftShift #-}


performReset ::
  forall (u :: Signa) (us :: [Signa]) (b :: *) (n :: Sig.Arity) (ps :: [*]).
  Has '[u] us ps =>
  u ('Sig.Reset n) b -> Sig.Dom (Scope us) n -> ps :> b
performReset op scope0 =
  Com.coerce $
  handleLoop Sig.isIn Ef.areEfs (Identity ())
    (Contef.coerceDomToIdentity scope0)
  where
  handleLoop ::
    forall us' ps' g. Functor g =>
    Sig.IsIn u us' ->
    Ef.AreEfs us' ps' ->
    g () ->
    Sig.Dom (Scope us' `Compose` g) n ->
    ps' :> g b
  handleLoop (Sig.There is) Ef.ConsAreEfs rollback scope =
    handleThere is rollback scope
  handleLoop Sig.Here Ef.ConsAreEfs rollback scope =
    handleHere op rollback scope
  handleLoop (Sig.Inject inj) Ef.ConsAreEfs rollback scope =
    handleHere (inj op) rollback scope

  handleThere ::
    forall u' us' t' r' ps' g.
    (Efs us' ps', Handler u' us' t', Functor g) =>
    Sig.IsIn u us' ->
    g () ->
    Sig.Dom (Scope (u' : us') `Compose` g) n ->
    Ef.MkEf u' t' r' : ps' :> g b
  handleThere is rollback scope =
    Comp $ ContT $ \k -> modifyEnv con k
    where
    con ::
      Env us' t' () ->
      (forall x qs. Efs us' qs => (qs :> t' qs (g x)) -> qs :> Env us' t' (g x)) ->
      ps' :> Env us' t' (g b)
    con rb tr =
      Com.coerce $
      handleLoop is Ef.areEfs (Compose $ rb $> rollback) scope'
      where
      scope' :: Sig.Dom (Scope us' `Compose` Compose (Env us' t') g) n
      scope' = Sig.mapDom scopeMap scope

      scopeMap ::
        forall a.
        Compose (Scope (u' : us')) g a ->
        Compose (Scope us') (Compose (Env us' t') g) a
      scopeMap (Compose (Scope x)) =
        Compose $ Scope (Com.coerce (tr (handle x)))

  handleHere ::
    forall u' us' t' r' ps' g.
    (Ef.Efs us' ps', Handler u' us' t', Functor g) =>
    u' ('Sig.Reset n) b ->
    g () ->
    Sig.Dom (Scope (u' : us') `Compose` g) n ->
    Ef.MkEf u' t' r' : ps' :> g b
  handleHere op' rollback scope =
    Comp $ ContT $ \k -> reset op' (Sig.mapDom scopeMap scope) rollback k
    where
      scopeMap :: 
        forall a.
        Compose (Scope (u' : us')) g a ->
        Nest t' g ps' a
      scopeMap (Compose (Scope x)) = Nest (handle x)
