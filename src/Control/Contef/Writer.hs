module Control.Contef.Writer
  ( Writer (..)
  , WriterC (..)
  , tell
  , intercept
  , listen
  , censor
  , pass
  , run
  , exec
  ) where

import Control.Contef (Ef, Efs, Has, (:>))
import qualified Control.Contef as Ef


data Writer :: * -> Ef.Signa where
  Tell :: forall s. s -> Writer s Ef.Shift ()
  Intercept :: forall s a. Writer s (Ef.Reset1 a) (s, a)


tell :: forall s us ps. (Monoid s, Has '[Writer s] us ps) => s -> ps :> ()
tell = Ef.performShift . Tell
{-# INLINE tell #-}


intercept ::
  forall s us a ps. (Monoid s, Has '[Writer s] us ps) =>
  (forall qs. Efs us qs => qs :> a) -> ps :> (s, a)
intercept x =
  Ef.performReset Intercept (Ef.Scope x Ef.:& Ef.Nildom)
{-# INLINE intercept #-}


listen ::
  forall s us a ps. (Monoid s, Has '[Writer s] us ps) =>
  (forall qs. Efs us qs => qs :> a) -> ps :> (s, a)
listen x = do
  (s, a) <- intercept x
  tell s
  pure (s, a)
{-# INLINE listen #-}


censor ::
  forall s us a ps. (Monoid s, Has '[Writer s] us ps) =>
  (s -> s) -> (forall qs. Efs us qs => qs :> a) -> ps :> a
censor f x = do
  (s, a) <- intercept x
  tell (f s)
  pure a
{-# INLINE censor #-}


pass ::
  forall s us a ps. (Monoid s, Has '[Writer s] us ps) =>
  (forall qs. Efs us qs => qs :> (s -> s, a)) -> ps :> a
pass x = do
  (s, (f, a)) <- intercept x
  tell (f s)
  pure a
{-# INLINE pass #-}


-- tell x >> tell y = tell (x `mappend` y)
-- listen (tell x >> c) >>= k
--  = listen c >>= \(s, a) -> k (mappend x s, a)


newtype WriterC (s :: *) (ps :: [*]) (a :: *) = WriterC { runWriterC :: (s, a) }


instance Monoid s => Ef.Carrier us (WriterC s) where
  type Env us (WriterC s) = (,) s

  modifyEnv scope k = do
    (s, x) <- scope (pure ()) Ef.coerce
    WriterC (s', x') <- k x
    let !s'' = mappend s s'
    pure $ WriterC (s'', x')

  unitCarrier x = pure (WriterC (mempty, x))
  {-# INLINE unitCarrier #-}


instance Monoid s => Ef.Handler (Writer s) us (WriterC s) where
  shift (Tell !s) k = do
    WriterC (s', a) <- k ()
    let !s'' = mappend s s'
    pure (WriterC (s'', a))
  {-# INLINE shift #-}

  reset Intercept (scope Ef.:& Ef.Nildom) _ k = do
    WriterC (t, x) <- Ef.getNest scope
    k (fmap ((,) t) x)


run ::
  forall s us ps a. (Monoid s, Efs us ps) =>
  (forall wr. Ef (Writer s) us wr => wr : ps :> a) -> ps :> (s, a)
run c = Ef.coerce (Ef.handle @_ @_ @(WriterC s) c)
{-# INLINE run #-}


exec ::
  forall s us ps a. (Monoid s, Efs us ps) =>
  (forall wr. Ef (Writer s) us wr => wr : ps :> a) -> ps :> s
exec c = fmap fst (run c)
{-# INLINE exec #-}
