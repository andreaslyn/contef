module Control.Contef.Nondet
  ( Nondet (..)
  , none
  , alt
  , decide
  , choose
  , NondetC (..)
  , run
  ) where

import Control.Contef (Ef, Efs, (:>))
import qualified Control.Contef as Ef

import qualified Data.Foldable as Fold


data Nondet :: Ef.Signa where
  None :: forall a. Nondet Ef.Shift a
  Decide :: Nondet Ef.Shift Bool


none :: forall a us ps. Ef.Has '[Nondet] us ps => ps :> a
none = Ef.performShift None
{-# INLINE none #-}


decide :: forall us ps. Ef.Has '[Nondet] us ps => ps :> Bool
decide = Ef.performShift Decide
{-# INLINE decide #-}


alt ::
  forall a us ps. Ef.Has '[Nondet] us ps =>
  (ps :> a) -> (ps :> a) -> ps :> a
alt s1 s2 = do
  b <- decide
  if b then s1 else s2
{-# INLINE alt #-}

-- none >>= k  =  none
-- alt none none  =  none
-- alt none (pure x) >>= k  =  k x
-- alt none c  =  alt c none
-- alt (alt c1 c2) c3  =  alt c1 (alt c2 c3)


choose :: forall a us ps. Ef.Has '[Nondet] us ps => [a] -> ps :> a
choose [] = none
choose (a:as) = alt (pure a) (choose as)


newtype NondetC (ps :: [*]) (a :: *) = NondetC { runNondetC :: [a] }


-- chooseAll k xs = Ef.coerce $ Fold.foldrM (\x acc -> fmap ((++ acc) . runNondetC) (k x)) [] xs
chooseAll ::
  forall a b us ps. Efs us ps =>
  (a -> ps :> NondetC ps b) -> [a] -> ps :> NondetC ps b
chooseAll k [] = pure (NondetC [])
chooseAll k (a : as) = do
  NondetC a' <- k a
  NondetC as' <- chooseAll k as
  pure (NondetC (a' ++ as'))


instance Ef.Carrier us NondetC where
  type Env us NondetC = []

  modifyEnv scope k = scope (pure ()) Ef.coerce >>= chooseAll k
  {-# INLINE modifyEnv #-}

  unitCarrier = pure . NondetC . pure
  {-# INLINE unitCarrier #-}

--   modifyEnv (\_ r -> r (unitCarrier x)) h
-- = Ef.coerce (unitCarrier x) >>= chooseAll h
-- = pure [x] >>= chooseAll h
-- = h x
--
--   modifyEnv (\_ r -> r s) unitCarrier
-- = Ef.coerce s >>= chooseAll unitCarrier
-- = [s1, s2, ...] >>= chooseAll unitCarrier
-- = pure $ NondetC $ [s1, s2, ...]
--
--   modifyEnv (\e _ -> pure (e $> x)) h
-- = pure ([()] $> x) >>= chooseAll h
-- = pure [x] >>= chooseAll h
-- = h x
--
--   modifyEnv (\e r -> fmap (fmap g) (f e r)) h
-- = fmap (fmap g) x >>= chooseAll h, where x = f e r
-- = x >>= pure . fmap g >>= chooseAll h
-- = x >>= \xs -> pure (fmap g xs) >>= chooseAll h
-- = x >>= \xs -> chooseAll h (fmap g xs)
--
-- Now it suffices to show that
-- chooseAll h (fmap g xs) = chooseAll (h . g) xs, for all xs.
--
-- Case xs = []. Both reduce to pure [].
-- Case xs = x : xs.
--   chooseAll h (fmap g (x : xs))
-- = chooseAll h (g x : fmap g xs)
-- = h (g x) >>= \x' ->
--   chooseAll h (fmap g xs) >>= \xs' ->
--   pure $ NondetC $ runNondetC x' ++ runNondetC xs'
-- = (h . g) x >>= \x' ->
--   chooseAll (h . g) xs >>= \xs' ->
--   pure $ NondetC $ runNondetC x' ++ runNondetC xs'
-- = chooseAll (h . g) (x : xs)


instance Ef.Handler Nondet us NondetC where
  shift Decide k = do
    xs <- k True
    ys <- k False
    pure $ NondetC (runNondetC xs ++ runNondetC ys)
  shift None _ = pure (NondetC [])
  {-# INLINE shift #-}

  reset x _ _ _ = case x of {}


run ::
  forall a us ps. Efs us ps =>
  (forall li. Ef Nondet us li => li : ps :> a) -> ps :> [a]
run x = Ef.coerce $ Ef.handle @_ @_ @NondetC x
{-# INLINE run #-}
