module Control.Contef.Reader
  ( Reader (..)
  , ReaderC (..)
  , ask
  , local
  , run
  ) where

import Control.Contef (Ef, Efs, Has, (:>))
import qualified Control.Contef as Ef

import Data.Functor.Identity (Identity (..))


data Reader :: * -> Ef.Signa where
  Ask :: forall s. Reader s Ef.Shift s
  Local :: forall s a. (s -> s) -> Reader s (Ef.Reset1 a) a


ask :: forall s us ps. Has '[Reader s] us ps => ps :> s
ask = Ef.performShift Ask
{-# INLINE ask #-}

-- (ask >>= \s -> ask >>= k s) = (ask >>= \s -> k s s)


local :: 
  forall s us ps a. Has '[Reader s] us ps =>
  (s -> s) -> (forall qs. Efs us qs => qs :> a) -> ps :> a
local m scope =
  Ef.performReset (Local m) (Ef.Scope scope Ef.:& Ef.Nildom)
{-# INLINE local #-}

-- local m (ask >>= k) = ask >>= local m . k . m
-- local m (local n c >>= k) = local (n . m) c >>= local m . k

--   local m (ask >>= k1) >>= k2
-- = pure $ ReaderC $ \s ->
--    (ask >>= k1) >>= runReaderC (m s) >>= k2 >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    (pure $ ReaderC $ \s' -> k1 s' >>= runReaderC s') >>=
--    runReaderC (m s) >>= k2 >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    (k1 (m s) >>= runReaderC (m s)) >>= k2 >>= runReaderC s
--
--   (ask >>= \t -> local m (k1 (m t))) >>= k2
-- = ask >>= \t -> local m (k1 (m t)) >>= k2
-- = pure $ ReaderC $ \s -> local m (k1 (m s)) >>= k2 >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    (pure $ ReaderC $ \s' ->
--      (k1 (m s)) >>= runReaderC (m s') >>= k2 >>= runReaderC s'
--    ) >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    (k1 (m s)) >>= runReaderC (m s) >>= k2 >>= runReaderC s


--   local m (local n x >>= k1) >>= k2
-- = pure $ ReaderC $ \s ->
--    (local n x >>= k1) >>= runReaderC (m s) >>= k2 >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    (pure $ ReaderC $ \s' -> x >>= runReaderC (n s') >>= k1 >>= runReaderC s'
--    ) >>= runReaderC (m s) >>= k1 >>= runReaderC s
-- = pure $ ReaderC $ \s ->
--    x >>= runReaderC (n (m s))
--      >>= k1 >>= runReaderC (m s) >>= k2 >>= runReaderC s
--
--   local (n . m) x >>= \t -> local m (k1 t) >>= k2
-- = (pure $ ReaderC $ \s ->
--      x >>= runReaderC (n (m s))
--        >>= (\t -> local m (k1 t) >>= k2)
--        >>= runReaderC s)
-- = (pure $ ReaderC $ \s ->
--      x >>= runReaderC (n (m s))
--        >>= (\t -> pure $ ReaderC $ \s' ->
--                    (k1 t) >>= runReaderC (m s') >>= k2 >>= runReaderC s')
--        >>= runReaderC s)
-- = pure $ ReaderC $ \s ->
--      x >>= runReaderC (n (m s))
--        >>= (\t -> (pure $ ReaderC $ \s' ->
--                      (k1 t) >>= runReaderC (m s') >>= k2 >>= runReaderC s'
--                   ) >>= runReaderC s)
-- = pure $ ReaderC $ \s ->
--      x >>= runReaderC (n (m s))
--        >>= (\t -> k1 t >>= runReaderC (m s) >>= k2 >>= runReaderC s)
-- = pure $ ReaderC $ \s ->
--      x >>= runReaderC (n (m s))
--        >>= k1 >>= runReaderC (m s) >>= k2 >>= runReaderC s


newtype ReaderC (s :: *) (ps :: [*]) (a :: *) = ReaderC (s -> ps :> a)


runReaderC :: forall s ps a. s -> ReaderC s ps a -> ps :> a
runReaderC s = \(ReaderC f) -> f s
{-# INLINE runReaderC #-}


instance Ef.Carrier us (ReaderC s) where
  type Env us (ReaderC s) = Identity

  modifyEnv scope k =
    pure $ ReaderC $ \s -> do
      x <- scope (Identity ()) (>>= Ef.coerce . runReaderC s)
      k' <- k (runIdentity x)
      runReaderC s k'

  unitCarrier a = pure (ReaderC $ \_ -> pure a)
  {-# INLINE unitCarrier #-}


instance Ef.Handler (Reader s) us (ReaderC s) where
  shift Ask k = pure $ ReaderC $ \s -> k s >>= runReaderC s
  {-# INLINE shift #-}

  reset (Local m) (r Ef.:& Ef.Nildom) _ k =
    pure $ ReaderC $ \s ->
      Ef.getNest r >>= (runReaderC $! m s) >>= k >>= runReaderC s
  {-# INLINE reset #-}


run ::
  forall s us ps a. Efs us ps =>
  (forall re. Ef (Reader s) us re => re : ps :> a) -> s -> ps :> a
run c !s = Ef.handle c >>= runReaderC s
{-# INLINE run #-}
