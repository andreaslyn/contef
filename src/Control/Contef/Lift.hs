{-# LANGUAGE AllowAmbiguousTypes #-} -- for liftIO

module Control.Contef.Lift
  ( Lift (..)
  , LiftC (..)
  , lift
  , liftIO
  , run
  ) where

import Control.Contef (Ef, (:>))
import qualified Control.Contef as Ef

import Control.Monad.IO.Class (MonadIO)
import qualified Control.Monad.IO.Class as MIO


data Lift :: (* -> *) -> Ef.Signa where
  Lift :: forall m a. Monad m => m a -> Lift m Ef.Shift a


lift ::
  forall (m :: * -> *) (a :: *) (us :: [Ef.Signa]) (ps :: [*]).
  (Monad m, Ef.Has '[Lift m] us ps) =>
  m a -> ps :> a
lift = Ef.performShift . Lift
{-# INLINE lift #-}


liftIO ::
  forall (m :: * -> *) (a :: *) (us :: [Ef.Signa]) (ps :: [*]).
  (MonadIO m, Ef.Has '[Lift m] us ps) =>
  IO a -> ps :> a
liftIO = lift @m . MIO.liftIO
{-# INLINE liftIO #-}


newtype LiftC (m :: * -> *) (ps :: [*]) (a :: *) = LiftC { runLiftC :: m a }


bind ::
  forall (m :: * -> *) (a :: *) (b :: *) (ps :: [*]).
  (Monad m, Ef.Efs '[] ps) =>
  (a -> ps :> LiftC m ps b) -> m a -> ps :> LiftC m ps b
bind k m = pure $ LiftC $ m >>= runLiftC . Ef.evalEfs . k


instance Monad m => Ef.Carrier '[] (LiftC m) where
  type Env '[] (LiftC m) = m

  unitCarrier = pure . LiftC . pure
  {-# INLINE unitCarrier #-}

  modifyEnv scope k = scope (pure ()) Ef.coerce >>= bind k
  {-# INLINE modifyEnv #-}


instance Monad m => Ef.Handler (Lift m) '[] (LiftC m) where
  shift (Lift m) k = bind k m
  {-# INLINE shift #-}

  reset x _ _ _ = case x of {}


run ::
  forall (m :: * -> *) (a :: *). Monad m =>
  (forall li. Ef (Lift m) '[] li => '[li] :> a) -> m a
run c = runLiftC $ Ef.eval $ Ef.handle c
{-# INLINE run #-}
