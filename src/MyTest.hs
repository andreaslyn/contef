module MyTest (testAllSols) where

import Control.Contef ((:>))
import qualified Control.Contef as Ef
import qualified Control.Contef.Except as Ex
import qualified Control.Contef.State as St

import Control.Monad (when)


maxSum :: Int
maxSum = 10 * 10 * 10 * 10 * 10 * 10


testAllSols ::
  Ef.Has
    -- Global state constant (A, B)
  '[St.State (Int, Int),
    -- increment fst or increment snd next.
    St.State Bool,
    -- Error reached maximal attempts
    Ex.Except String] ss ps =>
  -- Value to start search from.
  (Int, Int) ->
  ps :> (Int, Int) -- (x, y) such that Ax + By = 0
testAllSols (x, y) = do
  (a, b) <- St.get @(Int, Int)
  if a * x + b * y == 0
  then pure (x, y)
  else do
    when (x + y > maxSum) (Ex.throw "sum exceeded max")
    i <- St.get @Bool
    St.put (not i)
    if i
    then testAllSols (x + 1, y) >>= \(x', y') -> pure (x' + 1, y')
    else testAllSols (x, y + 1) >>= \(x', y') -> pure (x', y' + 1)
